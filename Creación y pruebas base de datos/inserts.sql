insert into tipoCuenta values(1, 'Cuenta de ahorros', 1, 0,0,0,0,0,0,0,0,0);
insert into tipoCuenta values(2, 'Cuenta Corriente Premium', 0, 50,100,35,75,0,1,0,35000,0);
insert into tipoCuenta values(3, 'Cuenta Corriente Corporativa', 0, 50,100,35,75,0,1,1,35000,0);

insert into TipoTransaccion values();

insert into persona values('12345678-9', 'Matías Zárate');
insert into persona values('11111111-9', 'Sebastián Godoy');
insert into persona values('8998765-1', 'Erik Astorga');
insert into persona values('22222222-2', 'Rosita Hormann');
insert into persona values('000000000000', 'Cristian Chiang');
insert into persona values('01010101111111', 'Gerente General de todo');

insert into cliente values('12345678-9','Calle Hermosa 201', 99999999, 1,'password123');
insert into cliente values('11111111-9','Avenida Siempre Viva 456', 88888888, 1,'godoy!!!');
insert into cliente values('8998765-1','Vivo en Cochimbo', 77777777, 1,'microondas');
insert into cliente values('22222222-2','Vicuñalandia', 66666666, 1,'toy resfriada :c');
insert into cliente values('000000000000','UCN', 55555555, 1,'profesorcito');

insert into gerente values('01010101111111');

insert into area values('Cochimbo', '01010101111111');
insert into area values('Vicuñalandia', '000000000000');

insert into rama values(1,'Cochimbo');
insert into rama values(2,'Cochimbo');
insert into rama values(3,'Vicuñalandia');

insert into sucursal values(11,'Larrondo',1);
insert into sucursal values(12,'Guayacán',1);
insert into sucursal values(13,'Sindempart',1);
insert into sucursal values(14,'Bosque San Carlos',1);
insert into sucursal values(21,'La Serena',1);
insert into sucursal values(22,'Cuatro Esquinas',1);
insert into sucursal values(23,'Playita',1);
insert into sucursal values(31,'El Durazno',1);
insert into sucursal values(32,'Gabriela Mistral',1);

insert into sucursalcorporativa values(21);

insert into sucursalminorista values(11);
insert into sucursalminorista values(12);
insert into sucursalminorista values(13);
insert into sucursalminorista values(14);
insert into sucursalminorista values(22);
insert into sucursalminorista values(23);
insert into sucursalminorista values(31);
insert into sucursalminorista values(32);

insert into cajeroautomatico values (1,'Al lado de la puerta',11);
insert into cajeroautomatico values (2,'En la plaza',12);
insert into cajeroautomatico values (3,'Subterráneo',13);
insert into cajeroautomatico values (4,'En el segundo piso',14);
insert into cajeroautomatico values (5,'Calle de bancos',21);
insert into cajeroautomatico values (6,'No sé c:',22);
insert into cajeroautomatico values (7,'Ahí',23);
insert into cajeroautomatico values (8,'en todos lados',31);
insert into cajeroautomatico values (9,'Al lado del museo',32);

--inserts cuentabancaria
insert into cuentabancaria values (111, '10-03-2017', null, 1);
insert into cuentabancaria values (222, '10-03-2017', null, 1);
insert into cuentabancaria values (333, '10-03-2017', null, 1);
insert into cuentabancaria values (444, '10-03-2017', null, 1);

insert into cuentacorriente values(111);
insert into cuentacorriente values(222);

insert into cuentacorrientepremium values(111,11);

insert into cuentacorrientecorporativa values(222,'000000000000',21);

insert into cuentacorrientepremiumcliente values(111,'12345678-9');
insert into cuentacorrientepremiumcliente values(111,'11111111-9');

insert into talonario values(1, 1, 70, 111);
insert into talonario values(2, 1, 70, 222);

insert into cheque values(11, 1, 1);
insert into cheque values(12, 1, 1);
insert into cheque values(13, 1, 1);
insert into cheque values(14, 1, 1);
insert into cheque values(21, 1, 2);
insert into cheque values(22, 1, 2);
insert into cheque values(23, 1, 2);
insert into cheque values(24, 1, 2);

insert into cuentaAhorro values(333, '8998765-1');
insert into cuentaAhorro values(444, '22222222-2');

--faltan los tipo de transaccion
insert into transaccion values(9090, '12-11-2017', 10000, 'Supermercado Merkat', , 11);
insert into transaccion values(9080, '13-10-2017', 90000, 'Deuda Internet', , 11);
insert into transaccion values(9070, '14-09-2017', 30000, 'Plata prestada', , 11);
insert into transaccion values(9060, '15-08-2017', 32000, 'Sodexo?', , 11);
insert into transaccion values(9050, '16-07-2017', 1000, 'Plata pa la micro', , 11);
insert into transaccion values(9040, '17-06-2017', 299990, 'Notebook', , 11);
insert into transaccion values(9030, '18-05-2017', 8000, 'Pago cuenta de la luz', , 11);
insert into transaccion values(9020, '19-04-2017', 15000, 'Pago cuenta del agua', , 11);

insert into cuentabancariatransaccion values();

insert into tarjeta values();

insert into tipocuentacantcheques values();