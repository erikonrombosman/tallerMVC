<?php
class Cliente extends Persona{
	private $rut;
	private $nombre;
	private $cuentas;
	private $telefono;
	private $direccion;
	
	function __constructor($rut){
		$stmt = Conexion::conectar()->prepare("SELECT rut, nombre, direccion, telefono, contraseña FROM cliente WHERE rutcliente = :rut");	
		$stmt->bindParam(":rut", $rut, PDO::PARAM_STR);
		$stmt->execute();

		$atributos = $stmt->fetch();
		$stmt->close();
		$this->rut = atributos["rut"];
		$this->nombre = atributos["nombre"];
		$this->telefono = atributos["telefono"];
		$this->direccion = atributos["direccion"];
		
		$stmt = Conexion::conectar()->prepare("SELECT count(rut) cantCuentas FROM cuentabancaria_cliente WHERE rutcliente = :rut");	
		$stmt->bindParam(":rut", $rut, PDO::PARAM_STR);
		$stmt->execute();
		
		$atributos = $stmt->fetch();
		
		$cuentas[$atributo[cantCuentas]];
		$stmt->close();
		
		$stmt = Conexion::conectar()->prepare("SELECT numcuenta cantCuentas FROM cuentabancaria_cliente WHERE rutcliente = :rut");	
		$stmt->bindParam(":rut", $rut, PDO::PARAM_STR);
		$stmt->execute();
		$count = 0;
		while($row = $stmt->fetch){
			$cuentas[$count] = new CuentaBancaria($row[numcuenta],$this);
			$count++;
		}
		$stmt->close();
		
	}
	function getTelefono (){
		return $this->telefono;
	}
	function getDireccion (){
		return $this->direccion;
	}
	function getCuentas(){
		return $this->cuentas;
	}
	function getRut(){
		return $this->rut;
	}
	function getNombre(){
		return $this->nombre;
	}
} 
?>