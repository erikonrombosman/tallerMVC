<?php

#EXTENSIÓN DE CLASES: Los objetos pueden ser extendidos, y pueden heredar propiedades y métodos. Para definir una clase como extensión, debo definir una clase padre, y se utiliza dentro de una clase hija.

require_once "conexion.php";

class Crud extends Conexion{

	#INGRESO USUARIO
	#-------------------------------------
	public static function ingresoUsuarioModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT rutCliente, password FROM $tabla WHERE rutCliente = :usuario");	
		$stmt->bindParam(":usuario", $datosModel["rut"], PDO::PARAM_STR);
		$stmt->execute();

		#fetch(): Obtiene una fila de un conjunto de resultados asociado al objeto PDOStatement. 
		
		return $stmt->fetch();
		$stmt->close();

	}

	public static function datosUserModel($rutCliente, $tabla){
		$stmt = Conexion::conectar()->prepare("SELECT direccion from $tabla where rutCliente = :rutCliente");
		$stmt->bindParam(":rutCliente", $rutCliente, PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->fectch();
		$stmt->close();
	}
}


?>