use stebanco;


create or replace table TipoCuenta (
codTipoCuenta integer,
descripcion varchar(50),
boolLibreta boolean,
boolEstadoCuentaMensual boolean,
cantChequesTalonariosIni integer,
cantChequesTalonariosSgtes integer,
cantChequesNotificacionIni integer,
cantChequesNotificacionSgte integer,
boolEstadocuentaPeriodo integer,
boolEstadoCuentaTrimestral integer,
cobroAnual integer,
cantChequesCobradosNuevaChequera integer,
primary key(codTipoCuenta));

create or replace table TipoTransaccion(
codTipoTrans integer,
nomTipoTrans varchar(50),
primary key(codTipoTrans));

create or replace table Persona(
rut varchar(12),
nombre varchar(50),
primary key(rut) );

create or replace table Cliente(
rutCliente varchar(12),
direccion varchar(50),
telefono integer,
boolDocumentos boolean,
contraseña varchar(20),
primary key(rutCliente) );

create or replace table Gerente(
rutGerente varchar(12),
primary key(rutGerente));

create or replace table Area(
nomArea varchar(50),
rutGerente varchar(12),
primary key(nomArea),
foreign key (rutGerente) references Gerente(rutGerente) );

create or replace table Rama(
codRama integer,
nomArea varchar(50),
primary key(codRama),
foreign key(nomArea) references Area(nomArea));

create or replace table Sucursal(
codSucursal integer,
direccion varchar(50),
codRama integer,
primary key(codSucursal),
foreign key(codRama) references Rama(codRama));


create or replace table SucursalCorporativa(
codSucursalCorporativa integer,
primary key(codSucursalCorporativa));


create or replace table SucursalMinorista(
codSucursalMinorista integer,
primary key(codSucursalMinorista));

create or replace table CajeroAutomatico(
codCajero integer,
ubicacion varchar(50),
codSucursal integer,
primary key(codCajero),
foreign key(codSucursal) references Sucursal(codSucursal));

create or replace table CuentaBancaria(
numCuenta integer,
fechaApertura date,
fechaCierre date,
codTipoCuenta integer,
primary key(numCuenta),
foreign key(codTipoCuenta) references TipoCuenta(codTipoCuenta));


create or replace table CuentaCorriente(
numCuenta integer,
primary key(numCuenta));

create or replace table  CuentaCorrientePremium(
numCuenta integer,
codSucursalMinorista integer,
primary key(numCuenta),
foreign key(codSucursalMinorista) references SucursalMinorista(codSucursalMinorista));

create or replace table CuentaCorrienteCorporativa(
numCuenta integer,
rutCliente varchar(12),
codSucursalCorporativa integer,
primary key(numCuenta),
foreign key (rutCliente) references Cliente(rutCliente),
foreign key (codSucursalCorporativa) references SucursalCorporativa(codSucursalCorporativa) );


create or replace table CuentaCorrientePremiumCliente(
numCuenta integer,
rutCliente varchar(12),
primary key(numCuenta, rutCliente),
foreign key (rutCliente) references Cliente(rutCliente));

create or replace table Talonario(
codTalon integer,
numIniCheque integer,
numFinCheque integer,
numCuentaCorriente integer,
primary key(codTalon),
foreign key(numCuentaCorriente) references CuentaCorriente(numCuenta));

create or replace table Cheque(
numCheque integer,
estadoChque boolean,
codTalon integer,
primary key(numCheque, codTalon),
foreign key(codTalon) references Talonario(codTalon));


create or replace table  CuentaAhorro(
numCuenta integer,
rutCliente varchar(12),
codSucursalMinorista integer,
primary key(numCuenta),
foreign key(codSucursalMinorista) references SucursalMinorista(codSucursalMinorista),
foreign key(rutCliente) references Cliente(rutCliente));

create or replace table Transaccion(
codTrans integer,
fechaTrans date,
montoTrans integer,
descripcion varchar(50),
codTipoTrans integer,
codSucursal integer,
codCajero integer,
primary key(codTrans),
foreign key(codSucursal) references Sucursal(codSucursal),
foreign key(codCajero) references CajeroAutomatico(codCajero),
foreign key(codTipoTrans) references TipoTransaccion(codTipoTrans));


create or replace table CuentaBancariaTransaccion(
numCuenta integer,
codTrans integer,
primary key(numCuenta, codTrans),
foreign key(codTrans) references Transaccion(codTrans) );



create or replace table  Tarjeta(
codTarjeta integer,
codCuentaBancaria integer,
primary key(codTarjeta),
foreign key(codCuentaBancaria) references CuentaBancaria(numCuenta));


create or replace table TipoCuentaCantCheques(
codTipoCuenta integer,
cantCheques integer,
primary key(codTipoCuenta, cantCheques),
foreign key(codTipoCuenta) references TipoCuenta(codTipoCuenta));




