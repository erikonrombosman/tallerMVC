<!DOCTYPE html>
<html lang="es">


<!--HEAD -->
 <?php include "head.php" ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">



  <!-- Navigation-->
 <?php include "navbar.php" ?>




  <!-- Contenido -->
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Inicio</a>
        </li>
        <li class="breadcrumb-item active">Pacientes</li>
      </ol>

      <!-- DataTable: Paciente-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Pacientes</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead id='thead-pacientes'></thead>
              <tfoot id='tfoot-pacientes'></tfoot>
              <tbody id='tbody-pacientes'></tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Actualizado hoy a las 09:50 AM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © ICCI UCN 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="modalLogOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Listo para irte?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Presiona "Logout" si estás listo para cerrar sesión.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- MODALS -->
    <!-- Modal: Eliminar Paciente -->
    <div class="modal fade" id="modal-eliminar-paciente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"">Ventana de Confirmación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ¿Estás por eliminar al paciente "<strong id='nombre-paciente'></strong>", desea continuar?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary">Confirmar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Inicializa algunos datos de ejemplo -->
    <script>
      $(document).ready(function(){
        var head = ['Rut', 'Nombres', 'Apellidos', 'Alergia', 'Modificar', 'Eliminar'];
        var head_data = '<tr>';
        for (var i = 0, n = head.length; i < n; i++) {  
          head_data += '<th>' + head[i] + '</th>';
        }
        head_data += '</tr>';
        $('#thead-pacientes').append(head_data);
        $('#tfoot-pacientes').append(head_data);
        //Algunos datos de paciente
        var pacientes = [
          { "rut": '17.679.133-0', "nombre": 'José', "ape": 'Espinoza Salas', "ale": 'Ninguna.'},
          { "rut": '17.456.431-k', "nombre": 'Erik', "ape": 'Astorga Bugueño', "ale": 'Alérgico a Dipirona'},
          { "rut": '16.666.666-6', "nombre": 'Diego', "ape": 'Cataldo Larraguibel', "ale": 'Ninguna.'},
          { "rut": '18.572.631-4', "nombre": 'Sebastián', "ape": 'Paz', "ale": 'Alérgico a Paracetamol y Dipirona'}
        ];

        for (var i = 0, n = pacientes.length; i < n; i++) {
          var row_data = '<tr>';
          row_data += '<td>' + pacientes[i].rut + '</td>';
          row_data += '<td>' + pacientes[i].nombre + '</td>';
          row_data += '<td>' + pacientes[i].ape + '</td>';
          row_data += '<td>' + pacientes[i].ale + '</td>';
          row_data += '<td>' + '<button class="btn btn-info btn-block">Editar</button>' + '</td>';
          row_data += '<td>' + '<button class="btn btn-danger btn-block eliminar-paciente">Eliminar</button>' + '</td>';
          row_data += '</tr>';            
          $('#tbody-pacientes').append(row_data);
        }
      });

      $('body').on('click', '.eliminar-paciente', function(){
        var cells = $(this).closest('tr').children('td');
        var nombre = cells.eq(1).text() + ' ' + cells.eq(2).text();
        $('#nombre-paciente').html(nombre);
        $('#modal-eliminar-paciente').modal('show');
      });

    </script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
  </div>
</body>

</html>
