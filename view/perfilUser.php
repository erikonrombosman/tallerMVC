<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<!--HEAD -->
 <?php include "head.php" ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
<?php include "navbar.php" ?>
  <!--FIN DE LAS BARRAS DE NAVEGACIÓN!!!! -->

<div class="content-wrapper">
    <div class="card card-reg-ind mx-auto mt-10">
      <div class="card-header">Perfil de Usuario</div>
      <div class="card-body">
        <form>
          <div class="form-group">
               <div class="col-md-12 form-row">
                  <div class= col-md-3>Dirección titular:</div>
                   <div class="col-md-9" id="direccion" > </div>
                </div>
                   </br>
              <div class="col-md-12 form-row">
               <div class= col-md-3>Rut:</div>
                <div class="col-md-9" id="rut"> <?php echo $_SESSION["rutCliente"]; ?> </div>
              </div>
               </br>

          </div>
          </br> </br>
          <div class="form group">
              <div class="col-md-12 form-row">
                <a class="col-md-5 btn btn-primary " data-toggle="modal" data-target="#cambiarClaveWeb">Cambiar Clave Web</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <a class="col-md-5 btn btn-primary " data-toggle="modal" data-target="#editarPerfil">Editar Perfil</a>
              </div>
            </br> 
              <div class="col-md-12 form-row">
                <a class="col-md-5 btn btn-primary "   data-toggle="modal" data-target="#cambiarClaveTarjeta">Cambiar Clave Tarjeta</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <a class="col-md-5 btn btn-primary "  data-toggle="modal" data-target="#pedirClaveUnica">Solicitar Clave única</a>
              </div>
          </div>
 
        </form>

      </div>
    </div> 
    </div>


    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>


    <!-- Cambiar clave web modal-->
    <div class="modal fade " id="cambiarClaveWeb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cambiar Clave Web</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class= "modal-body ">Ingrese clave web actual :</div>
           <input  class ="col-md-11  input-corrido" id="claveWeb" type="password" aria-describedby="emailHelp" placeholder="Clave web actual" >
           <div class= "modal-body ">Ingrese nueva clave web :</div>
           <input  class ="col-md-11  input-corrido" id="claveWebNew" type="password" aria-describedby="emailHelp" placeholder="Nueva Clave Web" >
          <div class= "modal-body ">Reingrese nueva clave web :</div>
           <input  class ="col-md-11  input-corrido" id="claveWeb" type="password" aria-describedby="emailHelp" placeholder="Confirme nueva clave" >
           <br>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" >Confirmar</a>
          </div>
        </div>
      </div>
    </div>


        <!-- editar perfil modal-->
    <div class="modal fade" id="editarPerfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- pedirClave_unica modal-->
    <div class="modal fade" id="pedirClaveUnica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

     <!-- Cambiar clave tarjeta modal-->
    <div class="modal fade " id="cambiarClaveTarjeta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cambiar Clave Tarjeta</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class= "modal-body ">Ingrese clave actual de su tarjeta :</div>
           <input  class ="col-md-11  input-corrido" id="claveWeb" type="password" aria-describedby="emailHelp" placeholder="Clave actual" >
           <div class= "modal-body ">Ingrese nueva clave de su tarjeta :</div>
           <input  class ="col-md-11  input-corrido" id="claveWebNew" type="password" aria-describedby="emailHelp" placeholder="Ingrese nueva Clave" >
          <div class= "modal-body ">Confirme nueva clave de su tarjeta :</div>
           <input  class ="col-md-11  input-corrido" id="claveWeb" type="password" aria-describedby="emailHelp" placeholder="Confirme nueva clave" >
           <br>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" >Confirmar</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
  </div>
</body>

</html>
