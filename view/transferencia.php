<!DOCTYPE html>
<html lang="en">

<!--HEAD -->
 <?php include "head.php" ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
<?php include "navbar.php" ?>
 <!-- Navigation-->
  <div class="content-wrapper">
    <div class="card card-reg-ind mx-auto mt-10">
      <div class="card-header">Realizar Transferencia</div>
        <div class="card-body">
          <form>
            <div class="form-group">
              <div class="col-md-12 form-row">
                  <div class= col-md-3>Nombre Banco:</div>
                    <select class="col-md-8 form-control" name="cargo">
                     <option>Banco del Pato Amarillo</option>
                     <option>SteVan Co.</option>
                     <option>Banco de Talca</option>
                     <option>Banco de los monitos de colores</option>
                     <option>Banco Lucksic</option>
                   </select>
              </div>
              </br>
              <div class="col-md-12 form-row">
               <div class= col-md-3>Número de Cuenta:</div>
                <input class="col-md-8" id="InputAlergias" type="text" aria-describedby="emailHelp" placeholder="Ingrese número de cuenta">
              </div>
              </br>
              <div class="col-md-12 form-row">
               <div class= col-md-3>Monto:</div>
                <input class="col-md-8" id="InputAlergias" type="text" aria-describedby="emailHelp" placeholder="Ingrese monto a transferir">
              </div>
              </br>
              <div class="col-md-12 form-row">
               <div class= col-md-3>Mensaje:</div>
                <input class="col-md-8" id="InputAlergias" type="text" aria-describedby="emailHelp" placeholder="Mensaje de la transferencia">
              </div>
              </br></br></br></br>
              <div class="col-md-12 form-row">
                <a class="col-md-5 btn btn-primary " href="login.html">Transferir</a>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <a class="col-md-5 btn btn-primary " href="login.html">Cancelar</a>
            </div>
          </form>
        </div>
      </div>
      <!-- Example DataTables Card-->

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
  </div>
</body>

</html>
